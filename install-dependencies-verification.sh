#!/usr/bin/env bash
set -e
set -x

apt-get install -y --no-install-recommends \
	enjarify \
	python3-pip python3-setuptools python3-wheel \
	python3-libarchive-c \
	libmagic1

# Install latest diffoscope (version in Debian stable is outdated)
pip3 install diffoscope

# Install latest apktool for verification debugging
wget --no-verbose https://bitbucket.org/iBotPeaches/apktool/downloads/apktool_2.3.3.jar -O /opt/apktool.jar
cat >/usr/local/bin/apktool <<EOF
#!/usr/bin/env bash
java -jar /opt/apktool.jar \$@
EOF
chmod +x /usr/local/bin/apktool
mkdir -p /root/.local/share/apktool/framework